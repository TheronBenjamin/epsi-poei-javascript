let currentPlayer = "tic";
let gameFinished = false;
const CHECKED_CLASSNAME = "checked";
const TIC_CLASSNAME = "tic";
const TAC_CLASSNAME = "tac";

initEvent();

function displayPlayer() {
    document.querySelector("#nomJoueur").textContent = currentPlayer;
}

function changePlayer() {
    if (currentPlayer === "tic") {
        currentPlayer = "tac";
    } else {
        currentPlayer = "tic";
    }
}

function getPlayerClassName() {
    return currentPlayer === "tac" ? TAC_CLASSNAME : TIC_CLASSNAME;
}

function getCodePossession(tttCase) {
    if (tttCase.classList.contains(CHECKED_CLASSNAME)) {
        if (tttCase.classList.contains(TIC_CLASSNAME)) {
            return 1;
        } else if (tttCase.classList.contains(TAC_CLASSNAME)){
            return -1;
        }
    }
    return 0;
}

function getResultsMatrice() {
    const resultsMatrice = [

    ];
    let ligne = 0;
    let colonne = 0;
    document.querySelectorAll(".case").forEach((tttCase) => {
        if (!resultsMatrice[ligne]) {
            resultsMatrice[ligne] = []
        }
        resultsMatrice[ligne][colonne] = getCodePossession(tttCase);
        colonne++;
        if (colonne > 2) {
            colonne = 0;
            ligne++;
        }
    });
    return resultsMatrice;
}

function checkWinnerByLines(resultsMatrice) {
    let winner = null;
    resultsMatrice.forEach((element) => {
        if (winner === null) {
            winner = getWinner(eval(element.join("+")))
        }
    });
    return winner;
}

function checkWinnerByColumn(resultsMatrice) {
    let winner = null;
    let sommeColonne = 0;
    for (let i = 0; i < resultsMatrice.length; i++) {
        for(let j = 0; j < resultsMatrice.length; j++) {
            // "j" itere sur les ligne "i" sur les colonnes
            sommeColonne += resultsMatrice[j][i];
        }
        winner = getWinner(sommeColonne)
        if (winner !== null) {
            break;
        }
        sommeColonne = 0;
    }
    return winner;
}

function checkWinnerByDiagonal(resultsMatrice) {
    let winner = null;
    let sommeDiagonale1 = 0;
    for (let i = 0; i < resultsMatrice.length; i++) {
        sommeDiagonale1 += resultsMatrice[i][i];
    }

    winner = getWinner(sommeDiagonale1)

    if (!winner) {
        let sommeDiagonale2 = 0
        for (let j = 0; j < resultsMatrice.length; j++) {
            sommeDiagonale2 += resultsMatrice[j][-j + resultsMatrice.length - 1]
            // [0] [-0 + (3-1)]
            // [1] [-1 + (3-1)]
            // [2] [-2 + (3-1)]
        }
        winner = getWinner(sommeDiagonale2)
    }
    return winner;
}

function getWinner(sum) {
    const winningResult = 3
    if (sum > 0 && Math.abs(sum) === winningResult) {
        return "tic";
    } else if (sum < 0 && Math.abs(sum) === winningResult) {
        return "tac";
    } else {
        return null;
    }
}

function checkWinner(resultsMatrice) {
    return checkWinnerByLines(resultsMatrice) || checkWinnerByColumn(resultsMatrice) || checkWinnerByDiagonal(resultsMatrice);
}

function checkEndOfGame() {
    const winner = checkWinner(getResultsMatrice());
    if (!winner) {
        changePlayer();
        displayPlayer();
    } else {
        alert(winner + " a gagné !");
        gameFinished = true;
    }
}

function initEvent() {
    displayPlayer();
    document.querySelectorAll(".case").forEach((tttCase) => {
        tttCase.addEventListener("click", (event) => {
            const element = event.target;
            if (!gameFinished && !element.classList.contains(CHECKED_CLASSNAME)) {
                element.classList.add(getPlayerClassName(), CHECKED_CLASSNAME);
                checkEndOfGame();
            }
        });
    });
}