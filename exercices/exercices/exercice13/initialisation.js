// cette fonction est auto appelante, elle ne s'exécutera que lorsque le DOM (l'arborescence HTML du document) sera totalement chargé

    // insérer le code ici
    // seules les actions liées à l'initialisation de l'application devront être intégrées dans ce fichier
    // le code aura une portée locale, uniquement à l'intérieur de cette fonction.
