const list = document.getElementById("list")

initList();

function changeButtonStatus(input) {
    document.getElementById("btnAdd").disabled = !(input.value !== null || input.value !== undefined || input.value !== "");
}

function initList() {
    ["Robin", "Superman", "Supergirl"]
        .forEach(element => createListElement(element))
}

function addNewItem() {
    const input = document.getElementById("superHeroNameInput")
    createListElement(input.value);
    input.value = null;
    document.getElementById("btnAdd").disabled = true;
}

function createListElement(elementText) {
    let li = document.createElement("li");

    // li.innerText = elementText;
    li.appendChild(document.createTextNode(elementText));
    li.appendChild(createRemoveButton(li))
    list.appendChild(li);
}

function remove(li) {
    list.removeChild(li)
}

function createRemoveButton(li) {
    let button = document.createElement("button");
    button.innerText = 'remove';
    button.addEventListener("click", () => remove(li));
    button.classList.add("ml-1");

    return button;
}

function reinitList() {
    // list.innerHTML = "";
    list.replaceChildren();
    initList()
}