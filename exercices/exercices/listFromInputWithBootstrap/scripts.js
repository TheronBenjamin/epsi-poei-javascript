const list = document.getElementById("list")

initList()

function initList() {
    ["Robin", "Superman"]
        .forEach(element => createListElement(element))
}

function addNewItem() {
    createListElement(document.getElementById("superHeroNameInput").value)
}

function createListElement(elementText) {
    let li = document.createElement("li");

    li.appendChild(document.createTextNode(elementText));
    li.appendChild(createRemoveButton(li))
    li.classList.add("list-group-item")
    list.appendChild(li);
}

function remove(node) {
    list.removeChild(node)
}

function createRemoveButton(target) {
    let button = document.createElement("button");
    button.innerText = "remove";
    button.classList.add("btn");
    button.classList.add("btn-danger");
    button.addEventListener("click", () => remove(target));

    return button;
}

function reinitList() {
    list.replaceChildren();
    initList()
}