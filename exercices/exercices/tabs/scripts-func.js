const TOGGLE_TAB_EVENT = new Event("toggle_tab");
const HIDE_TAB_EVENT = new Event("hide_tab");

document.querySelectorAll(".button-tab").forEach((tab) => {
    tab.addEventListener("click", (event) => {
        const targetId = getTargetId(event.target);
        closeCurrentOpenedTabIfNot(targetId);
        // Appel de la function openTargetTab(...) avec l'id recupérer dans les classes
        toggleTargetTab(targetId);
    });
});

function getTargetId(target) {
    // Recupération du second élément de la list de classe qui est l'id de la div à afficher
    return target.classList[1];
}

// Sleection de tout les élément qui corresponde au selecteur css ".tab-content"
// Pour chacun d'entre eux on ajoute un event listener
document.querySelectorAll(".tab-content").forEach((div) => {
    // On affiche la div ou on la cache en fonction de l'état précedent
    div.addEventListener("toggle_tab", (event) => toggleDivAndButtonStyle(div));
    // cache la div
    div.addEventListener("hide_tab", (event) => hideDivAndRemoveButtonStyle(div));
});

function toggleDivAndButtonStyle(div) {
    div.classList.toggle("closed")
    document.querySelector(".button-tab." + div.id).classList.toggle("active")
}

function hideDivAndRemoveButtonStyle(div) {
    div.classList.add("closed")
    document.querySelector(".button-tab." + div.id).classList.remove("active")
}

function closeCurrentOpenedTabIfNot(idTarget) {
    // document.querySelectorAll(".tab-content.opened)").forEach((div) => {
    document.querySelectorAll(".tab-content:not(.closed):not(#" + idTarget + ")").forEach((div) => 
        div.dispatchEvent(HIDE_TAB_EVENT)
    );
}

function toggleTargetTab(idTarget) {
    // Récup de l'élément d'id idTarget et lancement de l'event SHOW_TAB_EVENT 
    document.getElementById(idTarget).dispatchEvent(TOGGLE_TAB_EVENT);
}

