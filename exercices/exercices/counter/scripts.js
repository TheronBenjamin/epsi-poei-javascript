let counter;

resetCounter();
display();

function display() {
    document.getElementById("cnt").innerHTML = 
        `<span class="${getColor()}"><strong>${counter}</strong></span>`;
}

function increment() {
    counter++;
}

function decrement() {
    counter--;
}

function resetCounter() {
    counter = 0;
}

function getColor() {
    if (counter > 0) {
        return "green"
    } else if (counter < 0) {
        return "red"
    }
    return ""
}