const spanExpression = document.getElementById("expression");
const spanResult = document.getElementById("result");

document.querySelectorAll(".buttonClickable")
    .forEach(button => {
        button.addEventListener("click", event => {
    // spanExpression.textContent = spanExpression.textContent +""+ event.target.textContent;
    spanExpression.textContent = `${spanExpression.textContent}${event.target.textContent}`;
        });
}); 

document.querySelector("#buttonCalc")
    .addEventListener("click", event => {
        spanResult.textContent = "=" + eval(spanExpression.textContent);
    });

document.querySelector("#buttonReset")
    .addEventListener("click", event => {
        spanResult.textContent = "";
        spanExpression.textContent = "";
    });
