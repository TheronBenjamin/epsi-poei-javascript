const container = document.querySelector('.container');
const cases = document.querySelectorAll('.case');
const restart = document.querySelector('.restart');
const actions = document.querySelector('.actions');
let player = "X";
let winner = false;
let game = [];


playgGame();
restartGame();

function playgGame(){
    cases.forEach(cell => {
        cell.addEventListener('click', (event) =>{
            //Insérer X ou O dans la case
            cell.innerHTML = player;
            //Défini les couleurs en fonction du joueur
            changePlayerColor(player, cell);
            //Récupérer les valeur des data id de chaque cellules
            let cellIndex = cell.getAttribute('value');
            // Insérer le joueur actuel à l'id correspondant
            game[cellIndex] = player;
            //Vérifier si un joueur a gagner
            winnerCase();
            if(winner != true) {
                //Tester le joueur courrant et changer de joueur
                player === "X" ? player = "O" : player ="X";
                //Changer le text en fonction
                actions.innerHTML = `C'est au tour du joueur ${player}`;
                //Match null
                if(!game.length >= 9){
                    actions.innerHTML = `Match null`;
                }
            }
        })
    })
}

function changePlayerColor(player, cell) {
    player === "X" ? 
    cell.classList.add("red"):
    cell.classList.add("green") 
    player === "X" ? 
    cell.classList.remove("green"):
    cell.classList.remove("red")
}

function winnerCase(){
    const victoryMessage = `${player} a gagné la partie`;
    const firstRowWin = (game[0] === player && game[1] === player && game[2] === player);
    const secondRowWin = (game[3] === player && game[4] === player && game[5] === player);
    const thirdRowWin = (game[6] === player && game[7] === player && game[8] === player);
    const firstColWIn = (game[0] === player && game[3] === player && game[6] === player);
    const secondColWIn = (game[1] === player && game[4] === player && game[7] === player);
    const thirdColWIn = (game[2] === player && game[5] === player && game[8] === player);
    const firstDiag = (game[0] === player && game[4] === player && game[8] === player);
    const secondtDiag = (game[2] === player && game[4] === player && game[6] === player);
    
    if(
        firstRowWin||secondRowWin||thirdRowWin||
        firstColWIn||secondColWIn||thirdColWIn||
        firstDiag||secondtDiag
        ){
            winner = true;
            actions.innerHTML = victoryMessage;
        }
}

function restartGame(){
    restart.addEventListener("click", ()=>{
        player = "X";
        game = ["","","","","","","",""];
        cases.forEach(cell => {
            cell.innerHTML ="";
        })
    })
}
