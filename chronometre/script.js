const secondeHTML = document.getElementById('seconde')
const minuteHTML = document.getElementById('minute')
const dizaineSecondeHTML = document.getElementById('dizaine-seconde')
const button = document.getElementById('start')

let seconde = 1;
let dizaineSeconde = 1;
let minute = 1;

setInterval(chronometre, 1000);

function chronometre(){
    secondIncrement();
    if(seconde > 10){
        dizaineSecondeIncrement();
            if (dizaineSeconde > 6){
                minuteIncrement();
            }
    }
}

function secondIncrement(){
    secondeHTML.innerHTML = seconde++;
}

function dizaineSecondeIncrement(){
    seconde = 0
    secondeHTML.innerHTML = seconde++;
    dizaineSecondeHTML.innerHTML = dizaineSeconde++;
}

function minuteIncrement(){
    seconde = 0
    secondeHTML.innerHTML = seconde++;
    dizaineSeconde = 0;
    dizaineSecondeHTML.innerHTML = dizaineSeconde++;
    minuteHTML.innerHTML = minute++;
}